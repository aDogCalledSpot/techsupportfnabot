from BotInterface import BotInterface
from imapclient import IMAPClient
import time
import email
import yaml


class EmailForwarder:
    _bot = BotInterface()
    _client = None

    def __enter__(self):
        with open("config.yaml", 'r') as stream:
            try:
                cfg = yaml.load(stream)
                self._client = IMAPClient(host=cfg["host"])
                self._client.login(cfg["login"]["username"], cfg["login"]["password"])
            except yaml.YAMLError as exc:
                print(exc)
                exit(-1)
        self._client.select_folder('INBOX')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._client.logout()

    def _receive_mail(self):
        self._client.select_folder('INBOX')
        results = []

        messages = self._client.search('UNSEEN')
        if not messages:
            return results

        for uid, data in self._client.fetch(messages, b'RFC822').items():
            parsed_email = email.message_from_bytes(data[b'RFC822'])
            sender = parsed_email.get('From')
            subject = parsed_email.get('Subject')
            body = parsed_email.get_payload(0).get_payload()
            results.append((sender, subject, body))

        self._client.move(messages, 'READ')
        return results

    def run(self):
        while True:
            try:
                for sender, subject, message in self._receive_mail():
                    self._bot.send_message_to_chat(sender, subject, message)
                time.sleep(5)
            except KeyboardInterrupt:
                exit(-1)


with EmailForwarder() as forwarder:
    forwarder.run()

