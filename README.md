# TechSupportFNABot

This bot receives simple emails and forwards them into a designated
telegram chat.

To use the bot you will need to add a `config.yaml` to the top-level
of repository. This file is used to specify login and token data.

You will need to specify the following fields:

- `token`:  The APIToken for your Telegram bot given to you by the
             BotFather
- `host`:   The email host from which you want to receive emails.
             IMPORTANT: This program uses IMAP for the emails. 
             So make sure you have the IMAP host and not POP
- `login`:   The login for your email. It uses two fields
  - `username`: Your username (your email address)
  - `password`: The password for your email address
  
- `chatId`: The chatId of the Telegram chat to which the bot should forward the emails.
   
   

## Example layout for config.yaml:
```yaml
token:      123456:deadbeef-789

host:       imap.example.com

login:
  username: username@example.net
  password: aVeryS3cur3P455w0rd

chatId:     -123456789
```