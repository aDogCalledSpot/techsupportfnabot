#!/usr/bin/env python
# -*- coding: utf-8 -*-

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ParseMode
from telegram.ext import Updater, CommandHandler, MessageHandler, CallbackQueryHandler, Filters
from telegram.ext.dispatcher import run_async
import logging

import yaml

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


class BotInterface:
    _heading = "Someone requires assistance:\n\n"
    _updater = None
    _text = ""
    _chatId = 0

    # ensures no one accesses the bot who isn't allowed to
    def _check_chat_id(self, chat_id):
        return chat_id == self._chatId

    def _start(self, bot, update):
        # make sure the chat is correct
        if not self._check_chat_id(update.message.chat_id):
            update.message.reply_text("Sorry, you are not allowed access to this bot!")
            return

        update.message.reply_text("Seems this is the right chat. You're all set!")

    @run_async
    def _on_it_button(self, bot, update):
        # make sure the chat is correct
        query = update.callback_query
        if not self._check_chat_id(query.message.chat_id):
            return

        user = update.effective_user.first_name
        bot.edit_message_text(text=self._heading + self._text + "\n" + user + " is on it! 👍",
                              chat_id=query.message.chat_id,
                              message_id=query.message.message_id,
                              parse_mode=ParseMode.MARKDOWN)

    def _help(self, bot, update):
        # make sure the chat is correct
        if not self._check_chat_id(update.message.chat_id):
            return

        """Send a message when the command /help is issued."""
        update.message.reply_text('Help!')

    # dummy test method
    def _echo(self, bot, update):
        # make sure the chat is correct
        if self._check_chat_id(update.message.chat_id):
            return

        self._text = "```\n" + update.message.text + "\n```"
        keyboard = [[InlineKeyboardButton("I'm on it!", callback_data='1')]]
        message_markup = InlineKeyboardMarkup(keyboard)
        bot.send_message(chat_id=self._chatId, reply_markup=message_markup, parse_mode=ParseMode.MARKDOWN,
                         text=self._heading + self._text)

    def _error(self, bot, update, error):
        # make sure the chat is correct
        """Log Errors caused by Updates."""
        logger.warning('Update "%s" caused error "%s"', update, error)

    # sends a message to the FNA network group with added header
    def send_message_to_chat(self, sender, subject, message):
        self._text = "```\nFrom: " + sender + "\n\nSubject: " + subject + "\n\n" + message + "\n```"
        keyboard = [[InlineKeyboardButton("I'm on it!", callback_data='1')]]
        message_markup = InlineKeyboardMarkup(keyboard)
        self._updater.bot.send_message(chat_id=self._chatId, reply_markup=message_markup, parse_mode=ParseMode.MARKDOWN,
                                       text=self._heading + self._text)

    def __init__(self):
        # read the token
        with open("config.yaml", 'r') as stream:
            try:
                cfg = yaml.load(stream)
                # Create the EventHandler and pass it your bot's token.
                self._updater = Updater(cfg["token"])
                self._chatId = cfg["chatId"]
            except yaml.YAMLError as exc:
                print(exc)
                exit(-1)

        # on different commands - answer in Telegram
        self._updater.dispatcher.add_handler(CommandHandler("start", self._start))
        self._updater.dispatcher.add_handler(CallbackQueryHandler(self._on_it_button))
        self._updater.dispatcher.add_handler(CommandHandler("help", self._help))

        # on noncommand i.e message - echo the message on Telegram
        self._updater.dispatcher.add_handler(MessageHandler(Filters.text, self._echo))

        # log all errors
        self._updater.dispatcher.add_error_handler(self._error)

        # Start the Bot
        self._updater.start_polling()

    def __del__(self):
        # Run the bot until you press Ctrl-C or the process receives SIGINT,
        # SIGTERM or SIGABRT. This should be used most of the time, since
        # start_polling() is non-blocking and will stop the bot gracefully.
        self._updater.idle()
